import React, { Component } from 'react';
import './App.css';
import AddMovies from '../components/AddMovies/AddMovies'


class App extends Component {
  render() {
    return (
      <div className="App">
        <AddMovies />
      </div>
    );
  }
}

export default App;
