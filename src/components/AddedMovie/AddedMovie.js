import React, {Component} from 'react';
import DeleteBtn from '../DeleteBtn/DeleteBtn'

class AddedMovie extends Component {

    shouldComponentUpdate(nextProps) {
       return this.props.movieName !== nextProps.movieName
    }

    render(){
        return (
            <div className="movie">
                <input
                    type="text"
                    value={this.props.movieName}
                    className="input-movie"
                    onChange={this.props.change}
                />
                <DeleteBtn
                    delete={this.props.remove}
                />
            </div>
        );
    }
}

export default AddedMovie;