import React from 'react';

const DeleteBtn = (props) => {
    return (
        <button className='delete-btn' onClick={props.delete}>delete</button>
    );
};

export default DeleteBtn;