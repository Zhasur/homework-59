import React, {Component} from 'react';
import './AddMovies.css'
import AddedMovie from '../AddedMovie/AddedMovie'

class AddMovies extends Component {

    state = {
        movies: [],
        count: 0,
        name: ''
    };

    removeMovie = index => {
        const movies = [...this.state.movies];
        movies.splice(index, 1);
        let count = this.state.count;
        count--;

        this.setState({movies, count: count});
    };

    changeHandler(event) {
        this.setState({
            name: event.target.value
        })
    };

    changeAddedMovie(name, index) {

        let movies = this.state.movies;
        movies[index].movieName = name;

        this.setState({movies});
    }

    addMovie = () => {
        const movies = [...this.state.movies];
        const newMovie = {movieName: this.state.name, id: this.state.count};
        let count = this.state.count;

        if (newMovie.movieName !== '' && newMovie.movieName !== ' ') {
            movies.push(newMovie);
            ++count
        } else {
            alert('No task entered!')
        }

        this.setState({movies, count: count, name: ''});
    };

    render() {

        let movies = this.state.movies.map((movie, index) => {
            return (
                <AddedMovie
                    key={index}
                    movieName={movie.movieName}
                    remove={() => this.removeMovie(index)}
                    count={movie.id}
                    change={(e) => this.changeAddedMovie(e.target.value, index)}
                    newMovieName={this.state.name}
                />
            )
        });

        return (
            <div className="addMovies-block">
                <input
                    value={this.state.name}
                    id="movie-input"
                    onChange={(event) => this.changeHandler(event)}
                    type="text"
                    placeholder="Add some movie name"
                    className="input-movie"
                />
                <button
                    className="add-btn"
                    onClick={this.addMovie}
                >
                    add
                </button>
                <p>To watch list:</p>
                {movies}
            </div>
        );
    }
}

export default AddMovies;